ARG BASE_IMAGE=node:14.16-alpine

# ================================================================
# - the "builder" image installs npm packages
# - it produces artifacts (`/app` and `/root/.cache`) used by the
#   "cypress" and deploy images
# - the "builder" image is also used for running lint, tests, typescript
# ================================================================
FROM $BASE_IMAGE as builder

ENV NODE_ENV=test
ENV NEXT_TELEMETRY_DISABLED=1
RUN apk add --no-cache bash git
WORKDIR /app
COPY ./package.json ./
COPY ./package-lock.json ./
# run `npm ci` before adding app code for better Docker caching
# https://semaphoreci.com/docs/docker/docker-layer-caching.html
# `CI=true` suppresses Cypress progress log spam
RUN CI=true npm ci
COPY . ./
RUN NODE_ENV=production npm run build

# ================================================================
# the "cypress" image uses a different base image but the same app code
# ================================================================
FROM cypress/base:14.16.0 as cypress

WORKDIR /app
# copy cypress from the builder image
COPY --from=builder /root/.cache /root/.cache/
COPY --from=builder /app ./
ENV NODE_ENV=test
ENV NEXT_TELEMETRY_DISABLED=1

# ================================================================
# the deploy image is the same as the "builder" image except
# `NODE_ENV` is set to "production"
# ================================================================
FROM $BASE_IMAGE

WORKDIR /app
COPY --from=builder /app ./
ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1
EXPOSE 3000

CMD ["npm", "start"]
